package model.vo;

import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";

    private int tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private String from_station_name;
    private int endStationId;
    private String to_station_name;
    private String userType;
    private String gender;
    private int birthyear;
    private String identificador;

    public Trip(int tripId, LocalDateTime startTime, LocalDateTime stopTime, int bikeId, int tripDuration, int startStationId, String fromStation, int endStationId, String toStation, String UserType, String gender, int birth, String identificador) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.from_station_name = fromStation;
        this.endStationId = endStationId;
        this.to_station_name = toStation;
        this.userType = UserType;
        this.gender = gender;
        this.birthyear = birth;
        this.identificador = identificador;
    }

    @Override
    public int compareTo(Trip trip) {   	
    	return startTime.compareTo(trip.startTime);
    }

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getStopTime() {
		return stopTime;
	}

	public void setStopTime(LocalDateTime stopTime) {
		this.stopTime = stopTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public void setTripDuration(int tripDuration) {
		this.tripDuration = tripDuration;
	}

	public int getStartStationId() {
		return startStationId;
	}

	public void setStartStationId(int startStationId) {
		this.startStationId = startStationId;
	}

	public String getFrom_station_name() {
		return from_station_name;
	}

	public void setFrom_station_name(String from_station_name) {
		this.from_station_name = from_station_name;
	}

	public int getEndStationId() {
		return endStationId;
	}

	public void setEndStationId(int endStationId) {
		this.endStationId = endStationId;
	}

	public String getTo_station_name() {
		return to_station_name;
	}

	public void setTo_station_name(String to_station_name) {
		this.to_station_name = to_station_name;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getBirthyear() {
		return birthyear;
	}

	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}
	
	public String getIdentificador(){
		return identificador;
	}
	
	public void setIdentificador( String iden){
		identificador = iden;
	}
  
}
