package model.vo;

public class BikeRoute implements Comparable<BikeRoute>{

	private String tipo_Cicloruta;
	private String ruta;
	private String calle_referencia;
	private String calle_limite_extemo;
	private String otra_calle_limite_extremo;
	private double longitud;
	
	public BikeRoute(String tipo_Cicloruta, String ruta,
			String calle_referencia, String calle_limite_extemo,
			String otra_calle_limite_extremo, double longitud) {
		
		this.tipo_Cicloruta = tipo_Cicloruta;
		this.ruta = ruta;
		this.calle_referencia = calle_referencia;
		this.calle_limite_extemo = calle_limite_extemo;
		this.otra_calle_limite_extremo = otra_calle_limite_extremo;
		this.longitud = longitud;
	}
	
	
	
	public String getTipo_Cicloruta() {
		return tipo_Cicloruta;
	}
	public void setTipo_Cicloruta(String tipo_Cicloruta) {
		this.tipo_Cicloruta = tipo_Cicloruta;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getCalle_referencia() {
		return calle_referencia;
	}
	public void setCalle_referencia(String calle_referencia) {
		this.calle_referencia = calle_referencia;
	}
	public String getCalle_limite_extemo() {
		return calle_limite_extemo;
	}
	public void setCalle_limite_extemo(String calle_limite_extemo) {
		this.calle_limite_extemo = calle_limite_extemo;
	}
	public String getOtra_calle_limite_extremo() {
		return otra_calle_limite_extremo;
	}
	public void setOtra_calle_limite_extremo(String otra_calle_limite_extremo) {
		this.otra_calle_limite_extremo = otra_calle_limite_extremo;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	
	
	@Override
	public int compareTo(BikeRoute o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
