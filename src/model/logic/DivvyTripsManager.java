package model.logic;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.opencsv.*;

import model.vo.Bike;
import model.vo.Trip;
import model.data_structures.LinkedListQueue;
import model.data_structures.Node;
import model.data_structures.RedBlackBST;

public class DivvyTripsManager{

	LinkedListQueue<Trip> colaTrips = new LinkedListQueue<>();
	RedBlackBST<Integer, Bike> arbolBikes = new RedBlackBST<Integer, Bike>();
	
	public void loadTrips(String tripsFile) {

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while (nextLine != null) {
					int trip_id;
					LocalDateTime start_time;
					LocalDateTime end_time;
					int bikeId;
					int tripDuration;
					int from_station_id;
					String from_station_name;
					int to_station_id;
					String to_station_name;
					String userType;
					String gender;
					int birthyear;

					if (nextLine[0] != "") {
						trip_id = Integer.parseInt(nextLine[0]);
					} else {
						trip_id = 0;
					}
					DateTimeFormatter format;
					if(tripsFile == "./Data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q4.csv") {
						format = DateTimeFormatter
								.ofPattern("M/d/yyyy H:mm");
					}
					else{
						format = DateTimeFormatter
							.ofPattern("M/d/yyyy HH:mm:ss");
					}
					start_time = LocalDateTime.parse(nextLine[1], format);
					end_time = LocalDateTime.parse(nextLine[2], format);

					if (nextLine[3] != "") {
						bikeId = Integer.parseInt(nextLine[3]);
					} else {
						bikeId = 0;
					}
					if (nextLine[4] != "") {
						tripDuration = Integer.parseInt(nextLine[3]);
					} else {
						tripDuration = 0;
					}
					if (nextLine[5] != "") {
						from_station_id = Integer.parseInt(nextLine[5]);
					} else {
						from_station_id = 0;
					}

					from_station_name = nextLine[6];

					if (nextLine[7] != "") {
						to_station_id = Integer.parseInt(nextLine[7]);
					} else {
						to_station_id = 0;
					}

					to_station_name = nextLine[8];
					userType = nextLine[9];

					if (userType == "Subscriber") {
						gender = nextLine[10];
						birthyear = Integer.parseInt(nextLine[11]);
					} else {
						gender = "";
						birthyear = 0;
					}

					Trip trip = new Trip(trip_id, start_time, end_time, bikeId,
							tripDuration, from_station_id, from_station_name,
							to_station_id, to_station_name, userType, gender,
							birthyear, "");
					colaTrips.enqueue(trip);

					nextLine = reader.readNext();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
		System.out.println("Total trips cargados en el sistema: "
				+ colaTrips.getSize());
	}

	public void loadBikes(){
		for(Trip actual : colaTrips){
			int id = actual.getBikeId();
			if(arbolBikes.contains(id)){
				Bike bici = arbolBikes.get(id);
				bici.setTotalTrips();
				bici.setTotalDistance(distanceTraveled(actual));
				bici.setTotalDuration(actual.getTripDuration());		
			}
			else{
				Bike bici = new Bike(actual.getBikeId(), 1, distanceTraveled(actual), actual.getTripDuration());
				arbolBikes.put(bici.getBikeId(), bici);
			}
		}
		
		System.out.println("----------------------");
		System.out.println("N�mero total de bicicletas: " + arbolBikes.size());
		System.out.println("----------------------");
	
	}
	
	private int distanceTraveled(Trip actual) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Bike getBike(int id){
		Bike bici = arbolBikes.get(id);
		return bici;	
	}
	
	public LinkedListQueue<Integer> getBikeIdInRange(int Idmenor, int Idmayor){
		LinkedListQueue<Integer> queue = new LinkedListQueue<Integer>();
		queue = (LinkedListQueue<Integer>) arbolBikes.keysInRange(Idmenor, Idmayor);
		return queue;		
	}

	
	
	

	


}
