package model.data_structures;

import java.util.Iterator;

public class LinkedListQueue<T> implements ICola<T>{

	private Node<T> first;
	private Node<T> last;
	private int size=0;
	
	public LinkedListQueue(){
		first = null;
		last = null;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>{
		private Node<T> current = first;
		
		public boolean hasNext(){
			return current != null;
		}
		public void remove(){}
		public T next(){
			T item = current.getItem();
			current = current.getNext();
			return item;
		}
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void enqueue(T t) {
		Node<T> newNode = new Node<>(t);
		if(size==0){
			first = newNode;
			last = newNode;
		}
		else {
			Node<T> oldLast = last;
			oldLast.setNext(newNode);
			last = newNode;
		}
		size++;	
	}

	@Override
	public T dequeue() {
		if(size == 0){
			return null;
		}
		Node<T> oldFirst = first;
		T item = first.getItem();
		first = oldFirst.getNext();
		oldFirst.setNext(null);
		size--;
		return item;
	}
	
	public T getFirst(){
		return first.getItem();
	}
	
	public T getLast(){
		return last.getItem();
	}

	
}
