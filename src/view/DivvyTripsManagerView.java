package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.LinkedListQueue;
import model.vo.Bike;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadBikes();
					break;
					
				case 2:
					System.out.println("Ingrese el id de la bicicleta que desea buscar:");
					int id = Integer.parseInt(sc.next());
					
					Bike bici = Controller.getBike(id);
					
					if(bici == null) System.out.println("La bicicleta buscada no existe");
					
					else{
						System.out.println("LA INFORMACI�N DE LA BICICLETA CONSULTADA ES");
						System.out.println("Id: " + bici.getBikeId());
						System.out.println("Viajes totales: " + bici.getTotalTrips());
						System.out.println("Distancia total recorrida: " + bici.getTotalDistance());
						System.out.println("Duraci�n total de los viajes: " + bici.getTotalDuration());
					}			
					break;
					
				case 3:
					System.out.println("Ingrese el id menor del rango:");
					int IdMenor = Integer.parseInt(sc.next());
					
					System.out.println("Ingrese en id mayor del rango:");
					int IdMayor = Integer.parseInt(sc.next());
					
					LinkedListQueue<Integer> queueIds = Controller.getBikeIdInRange(IdMenor, IdMayor);
					System.out.println("LOS IDs ENCONTRADOS SON");
					for (Integer idActual : queueIds) 
					{
						System.out.println("--------");
						System.out.println( idActual );;
					}
					break;
					
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. Cree una nueva coleccion de bicicletas");
		System.out.println("2. Consultar informaci�n asociada a una bicicleta");
		System.out.println("3. Consultar los Ids de las bicicletas que se encuentren registradas con Ids en un rango");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
