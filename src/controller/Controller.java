package controller;


import model.data_structures.LinkedListQueue;
import model.logic.DivvyTripsManager;
import model.vo.Bike;


public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadBikes() {
		manager.loadTrips("./Data/Divvy_Trips_2017_Q1Q2/Divvy_Trips_2017_Q1.csv");	
		manager.loadTrips("./Data/Divvy_Trips_2017_Q1Q2/Divvy_Trips_2017_Q2.csv");	
		manager.loadTrips("./Data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q3.csv");	
		//manager.loadTrips("./Data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q4.csv");	
		
		manager.loadBikes();
	}
	
	public static Bike getBike(int id){
		return manager.getBike(id);
	}
		
	public static LinkedListQueue<Integer> getBikeIdInRange (int IdMenor, int IdMayor) {
		return manager.getBikeIdInRange(IdMenor, IdMayor);
	}
}
