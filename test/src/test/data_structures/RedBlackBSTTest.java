package test.data_structures;

import model.data_structures.RedBlackBST;
import junit.framework.TestCase;

public class RedBlackBSTTest extends TestCase{
	private RedBlackBST<String, String> arbol;
	
	public void setupEscenario1(){
		arbol = new RedBlackBST<String, String>();
		String[] arr = new String[10];
		arr[0] = "E";
		arr[1] = "A";
		arr[2] = "S";
		arr[3] = "Y";
		arr[4] = "Q";
		arr[5] = "U";
		arr[6] = "T";
		arr[7] = "I";
		arr[8] = "O";
		arr[9] = "N";
		
		for(int i=0; i<arr.length; i++){
			arbol.put(arr[i], arr[i]);
		}		
	}
	
	public void testSize(){
		setupEscenario1();
		assertEquals(10, arbol.size());
	}
	
	public void testIsEmpty(){
		setupEscenario1();
		assertTrue(!arbol.isEmpty());
	}
	
	public void testGet(){
		setupEscenario1();
		assertEquals("O", arbol.get("O"));
	}
	
	public void testGet2(){
		setupEscenario1();
		assertEquals(null, arbol.get("W"));
	}
	
	public void testGetHeight(){
		setupEscenario1();
		assertEquals(2, arbol.getHeight("Q"));
		assertEquals(2, arbol.getHeight("E"));
	}
	
	public void testContains(){
		setupEscenario1();
		assertTrue(arbol.contains("O"));
		assertFalse(arbol.contains("W"));
	}
	
	public void testPut(){
		setupEscenario1();
		arbol.put("W", "W");
		assertEquals(11, arbol.size());
	}
	
	public void testPut2(){
		setupEscenario1();
		assertEquals("S", arbol.getRoot());
	}
	
	public void testHeight(){
		setupEscenario1();
		assertEquals(4, arbol.height());
	}
	
	public void testMin(){
		setupEscenario1();
		try {
			assertEquals("A", arbol.min());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testMax(){
		setupEscenario1();
		try {
			assertEquals("Y", arbol.max());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public void testCheck(){
//		setupEscenario1();
//		assertEquals(true, arbol.check());
//	}
	
	
}
